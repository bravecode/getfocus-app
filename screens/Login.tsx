
import * as React from 'react';
import { StyleSheet, View, TextInput, TouchableHighlight, TouchableOpacity, Dimensions, Keyboard } from 'react-native';
import { Text } from '../components/Themed';

const SPACING = Layout.spacing;

import Layout from '../constants/Layout';
import { FontAwesome5 } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';

import Animated, {
  withSpring,
  useSharedValue,
  useAnimatedStyle,
  withTiming,
  Easing
} from 'react-native-reanimated';
import InputGroupContainer from '../components/InputGroupContainer';
import Input from '../components/Input';
import { useStore } from '../hooks/useStore';
import BaseColors from '../constants/BaseColors';


const height = Dimensions.get('screen').height;
const width = Dimensions.get('screen').width;
import { MaterialIcons } from '@expo/vector-icons';
const AnimatedView = Animated.createAnimatedComponent(View);

const Login = () => {
  const { appStore } = useStore();
  const { setIsSignIn } = appStore;
  const [hasAccount, setHasAccount] = React.useState(false);

  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');

  const generalTextSettings = {
    placeholderTextColor: BaseColors.gray,
    numberOfLines: 1,
    onSubmitEditing: Keyboard.dismiss,
  }

  return (
    <View style={[styles.container]}>
      <View style={styles.illustrationContainer}></View>
      <View style={[styles.authContainer]}>
        <Text style={[styles.verticalMargin, styles.authTitle]}>{!hasAccount ? 'Вход' : 'Регистрация'}</Text>
        <View style={[styles.verticalMargin]}>
          <InputGroupContainer>
            <Input
              onChangeValue={setUsername}
              addonBefore={<MaterialIcons name='alternate-email' size={24} color={BaseColors.gray} />}
              textInputProps={{
                ...generalTextSettings,
                placeholder: 'Email',
                value: username,
                textContentType: 'emailAddress'
              }} />
            <View style={{ height: 1, backgroundColor: BaseColors.gray }} />
            <Input
              onChangeValue={setPassword}
              addonBefore={<MaterialIcons name='lock-outline' size={24} color={BaseColors.gray} />}
              textInputProps={{
                ...generalTextSettings,
                placeholder: 'Пароль',
                value: password,
                secureTextEntry: true
              }} />
          </InputGroupContainer>
        </View>
        <TouchableOpacity style={[styles.button, styles.verticalMargin]} onPress={setIsSignIn}>
          <Text style={[{ color: BaseColors.white, fontSize: 24, lineHeight: 24 }]}>Войти</Text>
        </TouchableOpacity>
        <View style={[styles.centerContent, styles.verticalMargin]}>
          <Text>или через</Text>
        </View>
        <View style={[styles.verticalMargin, styles.socialContainer]}>
          {icons.map((icon) => {
            return (
              <TouchableOpacity key={icon} style={styles.iconContainer}>

                <FontAwesome5 name={icon} size={Layout.spacing * 3} />

              </TouchableOpacity>
            )
          })}
        </View>
        <View style={[{ width: '100%', justifyContent: 'center', flex: 1, flexDirection: 'row', position: 'absolute', bottom: Layout.spacing }]}>
          <Text>Первый раз у нас?</Text>
          <TouchableOpacity>
            <Text style={{ marginHorizontal: Layout.spacing / 2, color: BaseColors.red }}>Зарегестрируйтесь</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View >
  );
};

const icons = ['google', 'facebook', 'apple'];
export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: Layout.spacing * 2
  },
  centerContent: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  verticalMargin: {
    marginBottom: Layout.spacing * 2,
  },
  illustrationContainer: {
    flex: 0.5,
  },
  authTitle: {
    fontSize: 30,
    fontFamily: 'montserrat',
    lineHeight: 30
  },
  button: {
    height: 60,
    width: '100%',
    backgroundColor: BaseColors.red,
    borderRadius: Layout.spacing,
    justifyContent: 'center',
    alignItems: 'center',
  },
  socialContainer: {
    justifyContent: 'space-between',
    alignItems: 'stretch',
    alignContent: 'space-between',
    flexDirection: 'row'
  },
  iconContainer: {
    paddingLeft: Layout.spacing * 3,
    paddingRight: Layout.spacing * 3,
    paddingTop: Layout.spacing,
    paddingBottom: Layout.spacing,
    borderWidth: 1,
    borderColor: BaseColors.gray,
    borderRadius: Layout.spacing
  },
  authContainer: {
    flex: 1,
  }
});

