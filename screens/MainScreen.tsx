import * as React from "react";
import { StyleSheet, Text, TextInput, ScrollView } from "react-native";

import { View } from "../components/Themed";
import { RootTabScreenProps } from "../types";
import { Card } from "../components/Card";
import { IssueCards } from "../constants/IssueCards";
import Layout from "../constants/Layout";
import ListItems from "../components/ListItems";
import BaseColors from "../constants/BaseColors";

const spacing = Layout.spacing / 2;
const MainScreen = ({ navigation }: RootTabScreenProps<"Home">) => {

    return (
        <View style={styles.container}>
            <TextInput
                placeholder="Работа"
                style={styles.target}
                onChange={(val) => console.log(val)}
            ></TextInput>
            <ScrollView>
                <View style={styles.cardsContainer}>
                    <View style={styles.box}>
                        <View style={{ flex: 1, marginRight: spacing }}>
                            <Card {...IssueCards[0]} />
                        </View>
                        <View style={{ flex: 1, marginLeft: spacing }}>
                            <Card {...IssueCards[2]} />
                        </View>
                    </View>
                    <View style={styles.box}>
                        <View style={{ flex: 1, marginRight: spacing }}>
                            <Card {...IssueCards[1]} />
                        </View>
                        <View style={{ flex: 1, marginLeft: spacing }}>
                            <Card {...IssueCards[3]} />
                        </View>
                    </View>
                </View>
                <View style={{ flex: 1.5 }}>
                    <View style={styles.headerList}>
                        <Text style={styles.title}>Ближайшие задачи</Text>
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};

export default React.memo(MainScreen);

const styles = StyleSheet.create({
    target: {
        marginVertical: Layout.spacing,
        borderWidth: 3,
        borderRadius: Layout.spacing,
        height: Layout.spacing * 4,
        borderColor: BaseColors.Danger,
        paddingLeft: Layout.spacing,
    },
    container: {
        flex: 1,
        paddingLeft: Layout.spacing,
        paddingRight: Layout.spacing,
    },
    cardsContainer: {
        flexDirection: "column",
        height: 220,
    },
    headerList: {
        flexDirection: "row",
        justifyContent: "space-between",
        paddingVertical: Layout.spacing,
    },
    title: {
        fontFamily: "montserrat",
        fontSize: Layout.spacing * 2,
        lineHeight: Layout.spacing * 2,
    },
    box: {
        flex: 2,
        flexDirection: "row",
        justifyContent: "space-between",
        marginBottom: Layout.spacing,
    },
    separator: {
        marginVertical: Layout.spacing,
        height: 1,
    },
});
