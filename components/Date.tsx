import { Text } from "react-native"
import * as React from 'react';

const DateTitle = () => {
    const [date, setDate] = React.useState(new Date().toLocaleString())

    React.useEffect(() => {
        setTimeout(() => {
            setDate(new Date().toLocaleString())
        }, 1000)
    }, [date])

    return (
        <Text style={{ fontFamily: 'montserrat', letterSpacing: 1.5, fontSize: 16 }}>{date.toLocaleString()}</Text>
    )
}

export default DateTitle;