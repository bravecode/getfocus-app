import * as React from "react";
import { TextInput, StyleSheet, View, TextInputProps, Keyboard } from 'react-native';
import Layout from '../constants/Layout';
const SPACING = Layout.spacing;


interface ExtendetInputProps {
    addonBefore?: React.ReactNode,
    addonAfter?: React.ReactNode,
    onChangeValue: ((text: string) => void);
    textInputProps?: TextInputProps
}

const Input = ({ textInputProps, onChangeValue, addonBefore, addonAfter }: ExtendetInputProps) => {
    const paddingLeft = addonBefore ? Layout.spacing : 0;
    const paddingRight = addonAfter ? Layout.spacing : 0;

    return (
        <View style={styles.textInputContainer}>
            {addonBefore ? addonBefore : null}
            <TextInput {...textInputProps} style={[styles.input, { paddingLeft, paddingRight }]} onChangeText={(text) => onChangeValue(text)} />
            {addonAfter ? addonAfter : null}
        </View>
    )
}

const styles = StyleSheet.create({
    textInputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: Layout.spacing,
        borderColor: '#F3F3F3',

    },
    input: {
        flex: 1,
        lineHeight: 22,
        fontFamily: 'montserrat',
        fontSize: 22
    }
})

export default Input;