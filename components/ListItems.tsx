import * as React from 'react';
import { View, FlatList, Text, StyleSheet, TouchableOpacity, Vibration } from "react-native";
import Layout from '../constants/Layout';
import { observer } from 'mobx-react-lite';
import TaskStore from '../stores/TaskStore';
import { useStore } from '../hooks/useStore';
import BaseColors from '../constants/BaseColors';
import { AntdIcon } from './AntIcon';


const colors = [
    '#ee3d56',
    '#5396e3',
    '#f7bc58',
    '#37b5bd',
]

const alphaColors = [
    '#fae1e5',
    '#dbe9f9',
    '#fbf0dd',
    '#d9f1f0',
]

const ListItem = observer(({ task }: { task: TaskStore }) => {
    const { setActive, active } = task;

    const colorIndex = Math.floor(Math.random() * (0 - 4) + 4);

    return (
        <View style={{ flexDirection: 'row', borderBottomWidth: 1, borderColor: BaseColors.border }}>
            <View style={{ width: 18, height: 18, backgroundColor: colors[colorIndex], borderRadius: 4 }}></View>
            <TouchableOpacity onLongPress={() => { setActive(); Vibration.vibrate(100); }} style={[styles.listItem]}>
                <Text numberOfLines={1} style={[styles.taskTitle]}>
                    {task.title}
                </Text>
                <Text numberOfLines={1} style={[styles.taskDescription]}>
                    {task.description}
                </Text>
                <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center', paddingHorizontal: Layout.spacing }}>
                    <AntdIcon color={BaseColors.red} name='calendar' size={12} />
                    <Text style={[styles.taskDate]}>
                        {task.factDueDate ? new Date(+task.factDueDate).toLocaleDateString() : null}
                    </Text>
                </View>

            </TouchableOpacity>
        </View>
    )
})

const ListItems = observer((props) => {
    const { appStore } = useStore();

    const { computeTasks } = appStore;


    React.useEffect(() => {
        appStore.getTasks()
    }, [])

    const renderItem = ({ item }: { item: TaskStore }) => (<ListItem task={item} />)

    return (
        <FlatList showsVerticalScrollIndicator={false} ListFooterComponent={() => (<View style={{ height: 60 + Layout.spacing }} />)} contentContainerStyle={styles.listsContainer} ItemSeparatorComponent={() => <View style={styles.separator} />} data={computeTasks} renderItem={renderItem} keyExtractor={(item) => `${item.id}`} />
    )
})

const styles = StyleSheet.create({
    listsContainer: {

    },
    listItem: {
        flex: 1,
        height: 60,
        fontFamily: 'montserrat',
    },
    taskTitle: {
        fontFamily: 'montserrat',
        fontSize: 18,
        lineHeight: 18,
        color: BaseColors.black,
        paddingLeft: Layout.spacing,
        flexShrink: 1
    },
    taskDescription: {
        fontSize: 12,
        fontFamily: 'montserrat',
        lineHeight: 12,
        color: BaseColors.descText,
        paddingLeft: Layout.spacing,
        paddingTop: Layout.spacing / 2,
        flexShrink: 1
    },
    taskDate: {
        color: BaseColors.red,
        paddingLeft: Layout.spacing / 2,
    },
    indicatorContainer: {
        paddingLeft: Layout.spacing,
        paddingRight: Layout.spacing,
        // justifyContent: 'center',
        alignItems: 'center'
    },
    separator: {
        height: Layout.spacing
    }
});

export default ListItems;