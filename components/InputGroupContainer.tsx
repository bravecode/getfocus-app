import * as React from "react";
import { View, StyleSheet } from "react-native"
import BaseColors from "../constants/BaseColors";
import Layout from "../constants/Layout";


const InputGroupContainer: React.FC = ({ children }) => {
    return (
        <View style={[styles.container]}>
            {children}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        padding: 0,
        margin: 0,
        borderWidth: 1,
        borderColor: BaseColors.gray,
        borderRadius: Layout.spacing
    }
});

export default InputGroupContainer;