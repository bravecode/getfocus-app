import * as React from 'react';
import { StyleSheet, Text, TouchableOpacity, Animated, View } from 'react-native';
import Layout from '../constants/Layout';
import { Foundation } from '@expo/vector-icons';
interface CardProps {
    title?: string,
    count?: number,
    color: string,
    order: number,
    gradient: string,
    flex: number
}

const AnimatedView = Animated.createAnimatedComponent(View);

export const Card = (props: CardProps) => {
    const fadeAnim = React.useRef(new Animated.Value(0)).current;

    React.useEffect(() => {
        Animated.timing(
            fadeAnim,
            {
                toValue: 1,
                duration: props.order * 1000,
                useNativeDriver: true,
            }
        ).start();
    }, [fadeAnim]);

    const { flex, color, gradient } = props;

    return (
        <AnimatedView
            style={[styles.container, {
                height: 120,
                backgroundColor: color,
                flex: flex,
                shadowColor: color,
                opacity: fadeAnim,
            }]}>
            <View>
                <Text style={styles.countText}>{`${Math.ceil(Math.random() * (50 - 1) + 1)}`}</Text>
            </View>
            <View style={{ justifyContent: 'flex-end' }}>
                <View style={styles.circle} />
                <TouchableOpacity style={styles.icon}>
                    <Foundation name='plus' color="#FFF" size={24} />
                </TouchableOpacity>
            </View>

        </AnimatedView>
    )
}

const styles = StyleSheet.create({
    container: {
        padding: Layout.spacing,
        color: 'white',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignContent: 'space-between',
        borderRadius: Layout.spacing,
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.41,
        shadowRadius: 10,
        elevation: 4,
    },
    countText: {
        fontFamily: 'montserrat', color: '#FFF', lineHeight: 36, fontSize: 36, fontWeight: 'bold',
    },
    icon: {
        width: 36,
        height: 36,
        borderRadius: 36,
        alignItems: 'center',
        justifyContent: 'center'
    },
    circle: {
        position: 'absolute', width: 36, height: 36, borderRadius: 36, opacity: 0.15, backgroundColor: 'black'
    }
})