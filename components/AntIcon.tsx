
import { AntDesign } from '@expo/vector-icons';
import * as React from 'react';
import { IconType } from '../types';
export const AntdIcon = (props: {
    name: IconType;
    color?: string;
    size?: number,
    style?: object,
    onPress?: any
}) => {

    return <AntDesign {...props} />;
}

