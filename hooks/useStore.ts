import React from 'react';
import { stores } from '../stores/stores';

export const StoreContext = React.createContext(stores);

export const useStore = () => React.useContext(StoreContext);