/**
 * Learn more about using TypeScript with React Navigation:
 * https://reactnavigation.org/docs/typescript/
 */

import { AntDesign } from '@expo/vector-icons';
import { BottomTabScreenProps } from '@react-navigation/bottom-tabs';
import { CompositeScreenProps, NavigatorScreenParams } from '@react-navigation/native';
import { NativeStackScreenProps } from '@react-navigation/native-stack';

declare global {
  namespace ReactNavigation {
    interface RootParamList extends RootStackParamList { }
  }
}

export interface Task {
  id?: number;
  userId?: number;
  title?: string;
  description?: string;
  plannedTime?: string;
  spentTime?: string;
  active?: boolean
  important?: boolean,
  urgent?: boolean,
  plannedDueDate?: number,
  factDueDate?: number | string,
  status?: number,
  updatedAt?: string,
  createdAt?: string
}

export type TabItem = {
  route: 'Home' | 'List' | 'Stats' | 'Settings' | 'AddTask',
  label: any,
  icon: any,
  screen: any
}

export type RootStackParamList = {
  Root: NavigatorScreenParams<RootTabParamList> | undefined;
  Modal: undefined;
  Login: undefined;
};

export type RootStackScreenProps<Screen extends keyof RootStackParamList> = NativeStackScreenProps<
  RootStackParamList,
  Screen
>;

export type RootTabParamList = {
  Home: undefined;
  List: undefined;
  Stats: undefined;
  Settings: undefined;
  AddTask: undefined
};

export type RootTabScreenProps<Screen extends keyof RootTabParamList> = CompositeScreenProps<
  BottomTabScreenProps<RootTabParamList, Screen>,
  NativeStackScreenProps<RootStackParamList>
>;

export type IconType = React.ComponentProps<typeof AntDesign>['name'];
