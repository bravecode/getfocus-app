import * as React from 'react';
import { View, Dimensions, TouchableOpacity, Vibration, StyleSheet, Text } from 'react-native';
import {
    TabActions,
} from '@react-navigation/native';
import Svg, { Path } from "react-native-svg"
import { BottomTabBarProps, BottomTabNavigationOptions } from '@react-navigation/bottom-tabs';
import { AntdIcon } from '../components/AntIcon';
import BaseColors from '../constants/BaseColors';
import Layout from '../constants/Layout';

const height = Dimensions.get('screen').height;
const width = Dimensions.get('window').width;

const TAB_HEIGHT = 60;

const TabBar = ({ state, navigation, descriptors }: BottomTabBarProps) => {
    const tabWidth = width / state.routes.length;

    return (
        <View style={[{ flexDirection: 'row', position: 'absolute', bottom: 0, height: TAB_HEIGHT, backgroundColor: 'transparent' }]}>
            <Svg viewBox={`0 0 ${width} ${TAB_HEIGHT}`}>
                <Path
                    fill={'#F6F5F4'}
                    strokeWidth={1}
                    stroke={'#F6F5F4'}
                    d={`M 0 20
                        C 0 0 ${Layout.spacing * 2} 0 ${Layout.spacing * 2} 0 
                        L ${tabWidth * 2} 0 
                        C ${tabWidth * 2 - 6} ${TAB_HEIGHT} ${tabWidth * 3 + 6} ${TAB_HEIGHT} ${tabWidth * 3} 0 
                        L ${width - Layout.spacing * 2} 0
                        C ${width} 0 ${width} 20 ${width} 20
                        L ${width} 20
                        L ${width} ${TAB_HEIGHT} 
                        L ${0} ${TAB_HEIGHT} 
                        L 0 20 z`}
                />
            </Svg >
            <View style={{ position: 'absolute', width: width, height: TAB_HEIGHT, display: 'flex', flexDirection: 'row', backgroundColor: 'transparent' }}>
                {state.routes.map((route, index) => {
                    const { options }: { options: BottomTabNavigationOptions } = descriptors[route.key];
                    const isFocused = state.index === index;
                    const color = isFocused ? BaseColors.red : BaseColors.inActive;
                    if (index === 2) {
                        return (
                            <View key={route.key} style={[styles.tabContainer]}>
                                <TouchableOpacity onPress={() => console.log('qwr')}
                                    style={[styles.tabCenter, {
                                        width: tabWidth - 6,
                                        height: tabWidth - 6,
                                        borderRadius: tabWidth - 6,
                                        bottom: 26,
                                        borderWidth: 1,
                                        borderColor: '#F3F3F3'
                                    }]}>
                                    <AntdIcon size={30} color={BaseColors.liteBlue} name={options.icon} />
                                </TouchableOpacity>
                            </View>
                        )
                    }
                    return (
                        <View key={route.key} style={styles.tabContainer}>
                            <TouchableOpacity
                                style={{ justifyContent: 'center', alignItems: 'center' }}
                                onPress={() => {
                                    Vibration.vibrate(10);
                                    const event = navigation.emit({
                                        type: 'tabPress',
                                        target: route.key,
                                        canPreventDefault: true,
                                    });

                                    if (!event.defaultPrevented) {
                                        navigation.dispatch({
                                            ...TabActions.jumpTo(route.name),
                                            target: state.key,
                                        });
                                    }
                                }}>
                                <AntdIcon size={30} color={color} name={options.icon} />
                                <Text style={{ color: color, fontSize: 12 }}>{options.tabBarLabel}</Text>
                            </TouchableOpacity>
                        </View>
                    )
                })}
            </View>
        </View >
    );
}

const styles = StyleSheet.create({
    tabContainer: {
        flex: 1,
        height: TAB_HEIGHT,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent'
    },
    tabCenter: {
        backgroundColor: BaseColors.red,
        transform: [{ scale: 1 }],
        justifyContent: 'center',
        alignItems: 'center',
    }
})

export default TabBar;