/**
 * If you are not familiar with React Navigation, refer to the "Fundamentals" guide:
 * https://reactnavigation.org/docs/getting-started
 *
 */
import { BottomTabBarProps, createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer, DefaultTheme, DarkTheme } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as React from 'react';
import { ColorSchemeName, Pressable, StyleSheet, TouchableOpacity, View } from 'react-native';

import Layout from '../constants/Layout';
import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';

import ModalScreen from '../screens/ModalScreen';
import Login from '../screens/Login';


import { RootStackParamList, RootTabParamList } from '../types';
import LinkingConfiguration from './LinkingConfiguration';
import DateTitle from '../components/Date';
import { AntdIcon } from '../components/AntIcon';

import Tabs from '../constants/Tabs';
import BaseColors from '../constants/BaseColors';
import TabBar from './TabBar';
import { useStore } from '../hooks/useStore';
import { observer } from 'mobx-react';

const Navigation = observer(({ colorScheme }: { colorScheme: ColorSchemeName }) => {
  const { appStore } = useStore();

  return (
    <NavigationContainer
      linking={LinkingConfiguration}
      theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}>
      {appStore.isSignIn ? <RootNavigator /> : <Login />}
    </NavigationContainer >
  );
})

export default Navigation;

const Stack = createNativeStackNavigator<RootStackParamList>();

function RootNavigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Root" component={BottomTabNavigator} options={{ headerShown: false }} />

      <Stack.Group screenOptions={{ presentation: 'modal' }}>
        <Stack.Screen name="Modal" component={ModalScreen} />
      </Stack.Group>
    </Stack.Navigator>
  );
}


const BottomTab = createBottomTabNavigator<RootTabParamList>();


function BottomTabNavigator() {
  const colorScheme = useColorScheme();

  return (
    <BottomTab.Navigator
      initialRouteName="Home"
      tabBar={(props: BottomTabBarProps) => (<TabBar {...props} />)}
      screenOptions={({ navigation }: any) => ({
        tabBarStyle: styles.tabBar,
        tabBarActiveTintColor: Colors[colorScheme].tint,
        headerTitle: () => <DateTitle />,
        headerTitleAlign: 'center',
        headerStyle: {
          borderBottomWidth: 1,
          borderColor: BaseColors.border
        },
        headerRight: () => (
          <Pressable
            onPress={() => navigation.navigate('Modal')}
            style={({ pressed }) => ({
              opacity: pressed ? 0.5 : 1,
              marginRight: Layout.spacing * 2
            })}>
            <AntdIcon
              name="setting"
              size={32}
              color={"#6A6A6A"}
            />
          </Pressable>
        ),
      })}>
      {Tabs.map((tab: any) => {
        return (
          <BottomTab.Screen
            key={tab.route}
            name={tab.route}
            component={tab.screen}
            options={({ navigation }) => ({
              icon: tab.icon,
              tabBarLabel: tab.label,
              tabBarShowLabel: false,
              tabBarHideOnKeyboard: true
            })}
          />
        )
      })}
    </BottomTab.Navigator >
  );
}


const styles = StyleSheet.create({
  tabBar: {
    height: 60,
  }
})