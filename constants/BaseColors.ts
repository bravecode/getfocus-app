export default {
    Primary : '#23B097',
    Secondary : '#656BF0',
    Danger: '#E95050',
    Warning: '#E99950',
    Default: '#9FAAA8',

    red: '#ee3d56',
    blue: '#5396e3',
    orange: '#f7bc58',
    green: '#37b5bd',


    liteRed: '#fae1e5',
    liteBlue: '#dbe9f9',
    liteOrange: '#fbf0dd',
    liteGreen: '#d9f1f0',
    inActive: '#6A6A6A',
    border: '#F3F3F3',
    descText: '#B2B2B2',

    default: '#eee',
    white: '#fff',

    primary: '#637aff',
    primaryDark: '#2759ff',
    primaryLite: '#637aff99',
    black: '#000',
    gray: '#D4D4D4',
    
    accent: '#112233',
    green2: '#039a83',
    light: '#EEEEEE',
    dark: '#333',

    purple: '#8f06e4',
    skyBlue: 'skyblue',
    yellow: '#f8c907',
    pink: '#ff4c98',
    gold: 'gold',
    line: '#282C35',

    darkGray: '#999999',

    darkOverlayColor: 'rgba(0, 0, 0, 0.4)',
    lightOverlayColor: 'rgba(255, 255, 255, 0.6)',
    primaryAlpha: 'rgba(99, 122, 255, 0.15)',
    redAlpha: 'rgba(255, 84, 84, 0.15)',
    greenAlpha: 'rgba(96, 197, 168, 0.15)',
    purpleAlpha: 'rgba(146, 6, 228, 0.15)',
}