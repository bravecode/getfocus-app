import ListScreen from "../screens/ListScreen";
import MainScreen from "../screens/MainScreen";
import SettingsScreen from "../screens/SettingsScreen";
import StatsScreen from "../screens/StatsScreen";


export default [
    { route: 'Home', label: 'Главная', icon: 'home', screen: MainScreen },
    { route: 'List', label: 'Список', icon: 'calendar', screen: ListScreen },
    { route: 'Add', label: 'Добавить', icon: 'addfile', screen: ListScreen },
    { route: 'Stats', label: 'Статистика', icon: 'smileo', screen: StatsScreen },
    { route: 'Settings', label: 'Настройки', icon: 'piechart', screen: SettingsScreen },
]                                            