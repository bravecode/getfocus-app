import BaseColors from "./BaseColors"

interface CardItemType {
    title: string,
    color: string,
    order: number
}

const IssueCards = [
    { title: 'Срочное и важное', color: BaseColors.Danger, gradient: BaseColors.liteRed, order: 0, flex: 1 },
    { title: 'Не срочное и важное', color: BaseColors.Warning, gradient: BaseColors.liteOrange, order: 1, flex: 1 },
    { title: 'Срочное и не важное', color: BaseColors.Secondary, gradient: BaseColors.liteGreen, order: 2, flex: 1 },
    { title: 'Не срочное и не важное', color: BaseColors.Primary, gradient: BaseColors.liteBlue, order: 3, flex: 1 },
]

export { IssueCards, CardItemType }