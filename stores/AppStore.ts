import { observable, action, makeAutoObservable, computed } from "mobx";
import { Task } from "../types";
import TaskStore from "./TaskStore";
import data from './Data.json';

class AppStore {
    text: string = '';
    @observable isSignIn: boolean = false;
    @observable token: null | string = null;
    @observable tasks = observable<Array<TaskStore>>([]);

    constructor() {
        makeAutoObservable(this);
    }

    getTasks = () => {
        this.setTasks(data.data.map((item: Task) => new TaskStore(item)));
        // fetch('Data.json')
        //     .then(response => response.json())
        //     .then(result => {
        //         this.setTasks(result.map((item: Task) => new TaskStore(item)));
        //     })
        //     .catch(error => console.log('error', error));
    }

    @action setTasks = (data: any) => {
        this.tasks.replace(data);
    }

    @action setIsSignIn = () => {
        this.isSignIn = !this.isSignIn
    }

    @computed get computeTasks() {
        const alltasks: Array<TaskStore> = [];

        this.tasks.forEach((store: any) => {
            alltasks.push(store)
        });
        
        return alltasks;
    }

}


export default AppStore;