import { action, makeAutoObservable, observable } from "mobx";
import { Task } from "../types";

class TaskStore implements Task {
    @observable id = 0;
    @observable userId = 0;
    @observable title = '';
    @observable description = '';
    @observable plannedTime = ''
    @observable spentTime = '';
    @observable active = false;
    @observable important = false;
    @observable urgent = true;
    @observable plannedDueDate = 0;
    @observable factDueDate: number | undefined;
    @observable status?: number | undefined;
    @observable updatedAt?: string | undefined;
    @observable createdAt?: string | undefined;
    
    constructor(task: Task) {
        console.log(task)
        makeAutoObservable(this);
        Object.assign(this, task)

    }

    @action setId = (val: number) => {
        this.id = val;
    }

    @action setActive = () => {
        this.active = !this.active;
    }
}

export default TaskStore;